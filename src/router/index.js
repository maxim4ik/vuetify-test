import Vue from "vue";
import VueRouter from "vue-router";

import store from "../store";
import Login from "@/components/logins";
import Main from "@/components/mains";

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/");
};

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return;
  }
  next("/login");
};

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
    beforeEnter: ifAuthenticated
  },
  /*{
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function() {
      return import(/!* webpackChunkName: "about" *!/ "../views/About.vue");
    }
  },*/
  {
    path: "/login",
    name: "Login",
    component: Login,
    beforeEnter: ifNotAuthenticated
  }
];

const router = new VueRouter({
  routes
});

export default router;
