const mocks = {
  auth: { POST: { token: "This-is-a-mocked-token", userId: "1" } },
  user: {
    GET: {
      username: "Maxim B",
      role: "role",
      photo_path: "https://avatarko.ru/img/kartinka/2/cherep_kapyushon_uzhasy_1606.jpg"
    }
  }
};

const apiCall = ({ url, method }) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      try {
        resolve(mocks[url][method || "GET"]);
        console.log(`Mocked '${url}' - ${method || "GET"}`);
        console.log("response: ", mocks[url][method || "GET"]);
      } catch (err) {
        reject(new Error(err));
      }
    }, 1000);
  });

export default apiCall;
